import json

with open("config.json") as config_file:
    config = json.load(config_file);

# mysql_driver
import pymysql

mysql = config["mysql"]
db = pymysql.connect(mysql["host"], mysql["user"], mysql["password"], mysql["database"])
cursor = db.cursor()


def mysql_get_list_object(list_name):
    sql = "SELECT * FROM " + list_name
    cursor.execute(sql)
    results = cursor.fetchall()
    return results


def mysql_insert_post(post):
    index = post["id"].index('_')
    p_id = post["id"][index + 1:]
    g_id = post["id"][:index - 1]
    content = post["message"]
    created_time = post["updated_time"]
    post_value = (p_id, content, created_time, 0)
    group_has_posts = (g_id, p_id, 0)
    query = "INSERT INTO posts(p_id,p_content,created_time,status) VALUES (%s,%s,%s,%s)"
    cursor.execute(query, post_value)
    mysql_insert_group_has_posts(group_has_posts)
    db.commit()
    return cursor.lastrowid


def mysql_insert_group_has_posts(value):
    query = "INSERT INTO group_has_posts(group_id,post_id,status) VALUES (%s,%s,%s)"
    cursor.execute(query, value)
    db.commit()
    return cursor.lastrowid


# graph_API
import facebook

graph_config = config["graphAPI"]
graph = facebook.GraphAPI(access_token=graph_config["token"], version=graph_config["version"])


def graph_get_posts_by_id(post_id):
    objects = graph.get_object(id=post_id, fields="feed.limit(3)")
    return objects["feed"]["data"]
